# app.py

from flask import Flask, Response, request
import http.client
import json
from app import app
from packages.db import mongo
from packages.utils import string
from bson import json_util
from bson.objectid import ObjectId


STAGE_DNS = "stag.wallet.tpaga.co"

@app.route("/transactions", methods=["POST"])
def makeTransaction():
    body = request.get_json()
    productIds = body["productIds"]
    productIdsForFind = []
    for productId in productIds:
        productIdsForFind.append(ObjectId(productId))
        
    database = mongo.connect()
    productsCollection = database.products
    transactionsCollection = database.transactions
    transactionsCount = transactionsCollection.count()
    print(transactionsCount)
    products = productsCollection.find({'_id': { "$in": productIdsForFind}})
    
    productsForPurchase = []
    cost = 0
    for currentProduct in products:
        productsForPurchase.append({
            "name": currentProduct["name"],
            "value": str(currentProduct["numbersPrice"])
        })
        cost = cost + currentProduct["numbersPrice"]
    
    conn = http.client.HTTPSConnection(STAGE_DNS)
    idempotencyToken = string.new_idempotency_token()
    print(idempotencyToken)
    payload = json.dumps({
        "cost": cost,
        "purchase_details_url": "https://example.com/compra/348820",
        "voucher_url": "https://example.com/comprobante/348820",
        "idempotency_token": idempotencyToken,
        #"idempotency_token": "ccPnXXkKip",
        "order_id": str(transactionsCount + 1),
        "terminal_id": "sede_45",
        "purchase_description": "Compra en Tienda X",
        "purchase_items": productsForPurchase,
        "user_ip_address": "61.1.224.56",
        "expires_at": "2021-11-05T20:10:57.549653+00:00"
    })
    headers = {
        'Authorization': 'Basic YmFja2VuZC1qZWlzb24tY2FzdGFuZWRhOnNUYmtmVG1HN3dOclJuNzg=no-cache',
        'Cache-Control': 'no-cache',
        'Content-Type': 'application/json'
    }
    conn.request("POST", "/merchants/api/v1/payment_requests/create", payload, headers)
    res = conn.getresponse()
    data = res.read()
    response = data.decode("utf-8")
    responseJsonDumps = json.loads(response)
    
    if 'error_code' not in responseJsonDumps:
        transactionCreated = transactionsCollection.insert_one(responseJsonDumps).inserted_id
        return Response(
            json_util.dumps({'transactionId' : transactionCreated, 'redirectUrl': responseJsonDumps["tpaga_payment_url"], 'status': responseJsonDumps["status"]}),
            mimetype='application/json'
        )
    else:
        return Response(
            json_util.dumps({'code_error' : "TRANSACTION_DECLINED", "message_error": "Transaction declined", "responseInfo":  responseJsonDumps}),
            mimetype='application/json'
        )

@app.route("/transactions",methods=['GET'])
def getTransactions():
    database = mongo.connect()
    collection = database.transactions
    skip = request.args.get('skip') if (request.args.get('skip'))  else 0
    limit = request.args.get('limit') if (request.args.get('limit'))  else 1
    
    transactions = collection.find({}).skip(int(skip)).limit(int(limit))
    
    return Response(
        json_util.dumps({'transactions' : transactions}),
        mimetype='application/json'
)
    
    
@app.route("/transactions/detail",methods=['GET'])
def getTransactionDetail():
    conn = http.client.HTTPSConnection(STAGE_DNS)
    payload = ''
    headers = {
    'Authorization': 'Basic bWluaWFwcG1hLW1pbmltYWw6YWJjMTIz',
    'Cache-Control': 'no-cache',
    'Content-Type': 'application/json'
    }
    conn.request("GET", "/merchants/api/v1/payment_requests/pr-3d6a2289193bec5adb5080dc2e91cadeba29b58f06ebbba1aba4c9eb85c6777e76811dcd/info", payload, headers)
    res = conn.getresponse()
    data = res.read()
    return Response(
        json_util.dumps({'transactionDetail' : json.loads(data)}),
        mimetype='application/json'
)
    
@app.route("/transactions/revert",methods=['GET'])
def revertTransaction():
    conn = http.client.HTTPSConnection(STAGE_DNS)
    payload = json.dumps({
    "payment_request_token": "pr-3d6a2289193bec5adb5080dc2e91cadeba29b58f06ebbba1aba4c9eb85c6777e76811dcd"
    })
    headers = {
    'Authorization': 'Basic bWluaWFwcG1hLW1pbmltYWw6YWJjMTIz',
    'Cache-Control': 'no-cache',
    'Content-Type': 'application/json'
    }
    conn.request("POST", "/merchants/api/v1/payment_requests/refund", payload, headers)
    res = conn.getresponse()
    data = res.read()
    return Response(
        json_util.dumps({'transactionDetail' : json.loads(data)}),  
        mimetype='application/json'
)