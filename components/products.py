# app.py
import datetime
from flask import Flask,  Response, request
from packages.db import mongo
from bson.objectid import ObjectId
from bson import json_util
from app import app

@app.route("/products",methods=['GET'])
def getProducts():
    database = mongo.connect()
    collection = database.products
    skip = request.args.get('skip') if (request.args.get('skip'))  else 0
    limit = request.args.get('limit') if (request.args.get('limit'))  else 1
    
    products = collection.find({}).skip(int(skip)).limit(int(limit))
    
    return Response(
        json_util.dumps({'products' : products}),
        mimetype='application/json'
)

@app.route("/products",methods=['POST'])
def newProduct():
    product = request.get_json()
    product["createdAt"] = datetime.datetime.utcnow()
    database = mongo.connect()
    collection = database.products
    
    created = collection.insert_one(product).inserted_id
    
    return Response(
    json_util.dumps({'created' : created}),
    mimetype='application/json'
)
    
@app.route("/products",methods=['PUT'])
def updateProduct():
    product = request.json
    database = mongo.connect()
    collection = database.products
    productId = product["id"]
    
    updated = collection.update_one({'_id': ObjectId(productId)},{"$set": { 
            "name": product["name"],
            "price": product["price"],
            "sku": product["sku"],
            "image": product["image"],
            "category": product["category"],
            "numbersPrice": product["numbersPrice"],
        }}, upsert=True).matched_count
    
    return Response(
    json_util.dumps({'updated' : updated}),
    mimetype='application/json'
)
    
@app.route("/products",methods=['DELETE'])
def deleteProduct():
    product = request.json
    database = mongo.connect()
    collection = database.products
    productId = request.args.get('productId')
    
    deleted = collection.delete_one({'_id': ObjectId(productId)}).deleted_count
    
    return Response(
    json_util.dumps({'deleted' : deleted}),
    mimetype='application/json'
)